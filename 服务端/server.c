#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <dirent.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/types.h>

char buf[1024] = {};
void* ls(void* arg)
{
	int clifd=*(int*)arg;
	system("ls > .ls.txt");
	FILE* frp=fopen(".ls.txt","r");
	char buf1[1024]={};
	int k=0;
	while((buf1[k++]=fgetc(frp))!=EOF);
	buf1[k-1]='\0';
	fclose(frp);
	write(clifd,buf1,strlen(buf1)+1);
	system("rm -f .ls.txt");
}
void* cd(void* arg)
{
	int clifd=*(int*)arg;
	if(strstr(buf,"cd ")!=NULL)
	{
		char* ptr=strstr(buf,"cd ");
		int k=0;
		ptr+=3;
		char name[1024]={};
		while(*ptr!='\0')
		{
			name[k++]=*ptr++;
		}
		int flag=chdir(name);
		if(flag!=0)	
		{
			strcpy(buf,"error");
			write(clifd,buf,strlen(buf)+1);
		}
		else
		{	
			strcpy(buf,"success");
			write(clifd,buf,strlen(buf)+1);
		}
	}
	else
	{
		strcpy(buf,"error");
		write(clifd,buf,strlen(buf)+1);
	}
}
void* _mkdir(void* arg)
{
	int clifd=*(int*)arg;
	if(strstr(buf,"mkdir ")!=NULL)
	{
		char* ptr=strstr(buf,"mkdir ");
		int k=0;
		ptr+=6;
		char name[1024]={};
		while(*ptr!='\0')
		{
			name[k++]=*ptr++;
		}
		int flag=mkdir(name,0777);
		if(flag!=0)	
		{
			strcpy(buf,"error");
			write(clifd,buf,strlen(buf)+1);
		}
		else
		{	
			strcpy(buf,"success");
			write(clifd,buf,strlen(buf)+1);
		}
	}
	else
	{
		strcpy(buf,"error");
		write(clifd,buf,strlen(buf)+1);
	}
}
void* put(void* arg)
{
	int clifd=*(int*)arg;
	read(clifd,buf,sizeof(buf));
	if(strcmp(buf,"error")==0)
	{
		return NULL;
	}
	else
	{
		strcpy(buf,"start");
		write(clifd,buf,strlen(buf)+1);
		read(clifd,buf,sizeof(buf));
		FILE* frp=fopen(buf,"w");
		char buf1[1024]={};
		strcpy(buf1,"creat_success");
		write(clifd,buf1,strlen(buf1)+1);
		read(clifd,buf,sizeof(buf));
		int sum=0;
		int size=0;
		for(int i=0;i<strlen(buf);i++)
		{
			size=size*10+buf[i]-'0';
		}
		//printf("%d\n",size);
		while(size)
		{
			read(clifd,buf1,sizeof(buf1));
			size-=fwrite(buf1,1,size>sizeof(buf1)?sizeof(buf1):size,frp);
		}
		fclose(frp);
		strcpy(buf1,"success");
		write(clifd,buf1,strlen(buf1)+1);
	}
}
void* get(void* arg)
{
	int clifd=*(int*)arg;
	read(clifd,buf,sizeof(buf));
	int sum=0;
	FILE* frp=fopen(buf,"r");
	if(frp==NULL)
	{
		strcpy(buf,"fail");
		write(clifd,buf,strlen(buf)+1);
	}
	else
	{
		strcpy(buf,"find_success");
		write(clifd,buf,strlen(buf)+1);
		char buf1[10240]={};
		while((buf1[sum++]=fgetc(frp))!=EOF);
		buf1[sum-1]='\0';
		write(clifd,buf1,strlen(buf1)+1);
		fclose(frp);
	}
}
void* start_run(void* arg)
{
	int clifd=*(int*)arg;
	for(;;)
	{
		read(clifd,buf,sizeof(buf));
		if(strcmp("ls",buf)==0)
		{
			pthread_t pid;
			pthread_create(&pid,NULL,ls,&clifd);
			pthread_join(pid,NULL);
		}
		if(strstr(buf,"cd")!=NULL)
		{
			pthread_t pid;
			pthread_create(&pid,NULL,cd,&clifd);
			pthread_join(pid,NULL);
		}
		if(strstr(buf,"mkdir")!=NULL)
		{
			pthread_t pid;
			pthread_create(&pid,NULL,_mkdir,&clifd);
			pthread_join(pid,NULL);
		}
		if(strstr(buf,"put")!=NULL)
		{
			pthread_t pid;
			pthread_create(&pid,NULL,put,&clifd);
			pthread_join(pid,NULL);
		}
		if(strstr(buf,"get")!=NULL)
		{
			pthread_t pid;
			pthread_create(&pid,NULL,get,&clifd);
			pthread_join(pid,NULL);
		}
	}
}
int main()
{
	printf("服务器创建socket...\n");
	int sockfd = socket(AF_INET,SOCK_STREAM,0);
	if(0 > sockfd)
	{
		perror("socket");
		return -1;
	}

	printf("准备地址...\n");
	struct sockaddr_in addr = {};
	addr.sin_family = AF_INET;
	addr.sin_port = htons(7777);
	addr.sin_addr.s_addr = inet_addr("10.0.2.15");
	socklen_t len = sizeof(addr);

	printf("绑定socket与地址...\n");
	if(bind(sockfd,(struct sockaddr*)&addr,len))
	{
		perror("bind");
		return -1;
	}

	printf("设置监听...\n");
	if(listen(sockfd,5))
	{
		perror("listen");
		return -1;
	}

	printf("等待客户端连接...\n");
	for(;;)
	{
		struct sockaddr_in addrcli = {};
		int clifd = accept(sockfd,(struct sockaddr*)&addrcli,&len);
		if(0 > clifd)
		{
			perror("accept");
			continue;
		}
		pthread_t pid;
		pthread_create(&pid,NULL,start_run,&clifd);
	}
}

