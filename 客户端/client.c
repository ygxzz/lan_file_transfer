#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include "tools.h"
#include <sys/types.h>
#include <sys/stat.h>
int main()
{
	printf("服务器创建socket...\n");
	int sockfd = socket(AF_INET,SOCK_STREAM,0);
	if(0 > sockfd)
	{
		perror("socket");
		return -1;
	}

	printf("准备地址...\n");
	struct sockaddr_in addr = {};
	addr.sin_family = AF_INET;
	addr.sin_port = htons(7777);
	addr.sin_addr.s_addr = inet_addr("10.0.2.15");
	socklen_t len = sizeof(addr);

	printf("连接服务器...\n");
	if(connect(sockfd,(struct sockaddr*)&addr,len))
	{
		perror("connect");
		return -1;
	}
	printf("命令列表：\n");
	printf("ls\t\t显示服务器目录下的文件\n");
	printf("cd 目录名\t跳转到相应目录\n");
	printf("mkdir 目录名\t创建目录\n");
	printf("put 文件路径\t上传文件\n");
	printf("get 文件路径\t下载文件\n");
	for(;;)
	{
			char buf[1024] = {};
			printf("请输入命令\n");
			gets(buf);
			if(strcmp(buf,"quit")==0)
			{
				printf("通信结束!\n");
				break;
			}
			if(strcmp(buf,"ls")==0)
			{
				write(sockfd,buf,strlen(buf)+1);
				read(sockfd,buf,sizeof(buf));
				printf("%s",buf);
			}
			if(strstr(buf,"cd")!=NULL)
			{
				write(sockfd,buf,strlen(buf)+1);
				read(sockfd,buf,sizeof(buf));
				if(strcmp(buf,"error")==0)
				{
					printf("cd失败！\n");
				}
			}
			if(strstr(buf,"mkdir")!=NULL)
			{
				write(sockfd,buf,strlen(buf)+1);
				read(sockfd,buf,sizeof(buf));
				if(strcmp(buf,"error")==0)
				{
					printf("创建文件夹失败\n");
				}
			}
			if(strstr(buf,"put")!=NULL)
			{
				if(strstr(buf,"put ")!=NULL)
				{
					char* ptr=strstr(buf,"put ");
					ptr+=4;
					if(*ptr!='\0')
					{
						write(sockfd,buf,strlen(buf)+1);
					}
					char* tmp;
					int k=0;
					char path[4096]={};
					while(*ptr!='\0')
					{
						path[k++]=*ptr++;
					}
					//printf("%s\n",path);
					FILE* frp=fopen(path,"r");
					if(frp==NULL)
					{
						printf("找不到该文件！\n");
						strcpy(buf,"error");
						write(sockfd,buf,strlen(buf)+1);
					}
					else
					{
						write(sockfd,buf,strlen(buf)+1);
						read(sockfd,buf,sizeof(buf));
						if(strcmp(buf,"start")==0)
						{
							char filename[1024]={};
							int cnt=0;
							tmp=&path[0]-1;
							for(int i=0;i<k;i++)
							{
								if(path[i]=='/')
								{
									tmp=&path[i];
								}
							}
							tmp++;
							while(*tmp!='\0')
							{
								filename[cnt++]=*tmp++;
							}
							filename[cnt]='\0';
							struct stat s={};
							stat(path,&s);
							//printf("%u个字节\n",s.st_size);
							if(S_ISDIR(s.st_mode))
							{
								printf("无法上传目录！\n");
								strcpy(buf,"\0");
							}
							else
							{
								strcpy(buf,filename);
								write(sockfd,buf,strlen(buf)+1);
								read(sockfd,buf,sizeof(buf));
								//printf("%s\n",buf);
								if(strcmp(buf,"creat_success")==0)
								{
									char buf1[1024]={};
									sprintf(buf,"%d",(int)s.st_size);
									//printf("%s",buf);
									write(sockfd,buf,strlen(buf)+1);
									int sum=(int)s.st_size;
									while(sum)
									{
										sum-=fread(buf,1,sum>sizeof(buf)?sizeof(buf):sum,frp);
										write(sockfd,buf,sizeof(buf));
									}
									fclose(frp);
									read(sockfd,buf,sizeof(buf));
									if(strcmp(buf,"success")==0)
									{
										printf("上传成功！\n");
									}
								}
							}
						}
					}
				}
				else
				{
					printf("put命令错误！\n");
				}
			}
			if(strstr(buf,"get")!=NULL)
			{
				if(strstr(buf,"get ")!=NULL)
				{
					char* ptr=strstr(buf,"get ");
					ptr+=4;
					if(*ptr!='\0')
					{
						write(sockfd,buf,strlen(buf)+1);
					}
					int k=0;
					char filename[1024]={};
					while(*ptr!='\0')
					{
						filename[k++]=*ptr++;
					}
					FILE* frp=fopen(filename,"w");
					strcpy(buf,filename);
					write(sockfd,buf,strlen(buf)+1);
					read(sockfd,buf,sizeof(buf));
					if(strcmp(buf,"find_success")==0)
					{
						char data[10240]={};
						read(sockfd,data,sizeof(data));
						fprintf(frp,"%s",data);
						printf("下载成功！\n");
						fclose(frp);
					}
					else
					{
						printf("该文件不存在！\n");
						char tmp[1024]={};
						sprintf(tmp,"rm -f %s",filename);
						system(tmp);
						fclose(frp);
						strcpy(buf,"\0");
					}
				}
				else
				{
					printf("get命令错误！\n");
				}
			}
	}
	close(sockfd);
}

