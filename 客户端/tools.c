#include "tools.h"
#include<string.h>
#include<getch.h>
#include<stdbool.h>
char* get_str(char* str,size_t len)
{
	if(NULL==str)
	{
		puts("空指针异常!");
		return NULL;
	}

	int n=fgets(str,len,stdin);
	size_t cnt = strlen(str);
	if('\n'==str[cnt-1])
	{
		str[cnt-1]='\0';
	}
	clear_stdin();
	return str;

}

void clear_stdin(void)
{
	stdin->_IO_read_ptr=stdin->_IO_read_end;
}

char get_sex(void)
{
	printf("请输入性别(m男,w女):");
	while(true)
	{
		char sex = getch();
		if('m'==sex||'w'==sex)
		{
			printf("%s\n",'w'==sex?"女":"男");
			return sex;
		}
	}
}
char get_cmd(char start,char end)
{
	clear_stdin();
	printf("请输入指令:");
	while(true)
	{
		char cmd=getch();
		if(cmd>=start&&cmd<=end)
		{
			printf("%c\n",cmd);
			return cmd;
		}
	}
}
