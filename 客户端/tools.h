#ifndef TOOL_H
#define TOOL_H

#include<stdio.h>

char* get_str(char* str,size_t len);

void clear_stdin(void);

char get_sex(void);

char get_cmd(char start,char end);
#endif//TOOL_H

